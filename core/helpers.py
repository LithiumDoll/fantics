import os
import random

from enum import Enum

from django.utils.html import strip_tags

from bs4 import BeautifulSoup


def teaser(value, length=800):
    if not value:
        return ''

    text = ''

    if strip_tags(value) != value:
        html = BeautifulSoup(value, 'html.parser')
        if html.find('p'):
            text = html.find('p').text
    else:
        text = value

    if not text:
        return ''

    text = strip_tags(text)

    return ('{}...'.format(text[:length])) if len(text) > length else text


def file_name(filename):
    try:
        extension = os.path.splitext(filename.lower())[1]
    except (AttributeError, IndexError):
        extension = ".jpg"

    extension = extension.lower()

    filename = ''.join((random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'))
                       for x in range(10))

    return '{}{}'.format(filename, extension)


class EnumChoices(Enum):
    @classmethod
    def choices(cls):
        return [x.value for x in cls]

    @classmethod
    def get_value(cls, key):
        try:
            return cls[key].value[0]
        except KeyError:
            return None

    @classmethod
    def label(cls, key):
        if isinstance(key, int):
            for x in cls:
                if x.value[0] == key:
                    return x.value[1]

        else:
            try:
                return cls[key].value[1]
            except KeyError:
                return None

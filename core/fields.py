import ast

from django import forms
from django.core.exceptions import ValidationError


class MultipleChoiceField(forms.ChoiceField):
    def validate(self, value):
        """Validate that the input is in self.choices."""
        try:
            values = list(map(int, ast.literal_eval(value)))
        except (ValueError, TypeError, SyntaxError):
            values = []

        choices = [k for k, v in self.choices]

        if values and not set(values).issubset(choices):
            raise ValidationError(
                self.error_messages['invalid_choice'],
                code='invalid_choice',
                params={'value': value},
            )

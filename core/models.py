import requests

from decimal import Decimal
from json.decoder import JSONDecodeError
from requests.exceptions import ConnectionError

from django.conf import settings
from django.db import models


class Currency(models.Model):
    last_update = models.DateTimeField(auto_now=True)
    rate = models.DecimalField(max_digits=30, decimal_places=20)
    code = models.CharField(max_length=3)

    def convert(self, amount, to):
        to = to.upper()

        if not isinstance(amount, Decimal):
            amount = Decimal(amount)

        if to == self.code:
            return amount

        return round(amount * Decimal(self.rate), 2)

    @classmethod
    def update(cls):
        url = 'https://openexchangerates.org/api/latest.json?app_id={}'.format(
            settings.OPEN_EXCHANGE_APP_ID
        )

        response = requests.get(url)

        try:
            data = response.json()
        except (JSONDecodeError, ConnectionError, AttributeError, Exception):
            data = {}

        if data:
            if 'rates' in data:
                Currency.objects.all().delete()

                rates = [Currency(code=code, rate=rate) for code, rate in data['rates'].items()]

                Currency.objects.bulk_create(rates)

                return rates

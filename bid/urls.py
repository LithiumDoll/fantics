from django.urls import path

from .views import (BidListView, BidWinView, BidUpdateView, BidDeleteView, BidEmailView, BidValidateView,
                    BidRemovePinchHitterView, BidAlertEmailView, BidConfirmEmailView, BidBulkAlertEmailView,
                    BidDonationView)


app_name = 'bid'

urlpatterns = [
    path('<slug:user>/<slug:auction>/results/bulk-email/', BidBulkAlertEmailView.as_view(), name='email_bulk_alert'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/email/', BidEmailView.as_view(), name='email'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/email/alert/', BidAlertEmailView.as_view(), name='email_alert'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/email/confirm/',
         BidConfirmEmailView.as_view(to_winner=True, to_auctionee=True),
         name='email_confirm'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/email/confirm/winner/', BidConfirmEmailView.as_view(to_winner=True), name='email_confirm_to_winner'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/email/confirm/auctionee/', BidConfirmEmailView.as_view(to_auctionee=True), name='email_confirm_to_auctionee'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/validate/', BidValidateView.as_view(), name='validate'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/remove-pinch-hit/',
         BidRemovePinchHitterView.as_view(),
         name='remove_pinch_hit'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/edit/', BidUpdateView.as_view(), name='update'),
    path('<slug:user>/<slug:auction>/results/<int:pk>/delete/', BidDeleteView.as_view(), name='delete'),
    path('<slug:user>/<slug:auction>/<slug:bidder>/bids/', BidListView.as_view(), name='list'),
    path('<slug:user>/<slug:auction>/<slug:bidder>/wins/', BidWinView.as_view(), name='wins'),
    path('<slug:user>/<slug:auction>/<slug:bidder>/wins/<slug:hash>/', BidDonationView.as_view(), name='donate'),
]

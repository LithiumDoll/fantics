from django import forms

from account.models import User

from .models import Bid


class BidForm(forms.ModelForm):
    class Meta:
        model = Bid
        fields = ['amount', 'anonymous']


class BidAdministrationForm(forms.ModelForm):
    donation_deadline = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)
    pinch_hitter = forms.CharField(widget=forms.EmailInput(attrs={'placeholder': "E-mail address"}),
                                   required=False)

    class Meta:
        model = Bid
        fields = ['pinch_hitter', 'donation_deadline', 'receipt', 'has_donated']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.instance.pinch_hitter:
            self.fields['pinch_hitter'].widget = forms.HiddenInput()

    def clean_pinch_hitter(self):
        if not self.cleaned_data.get('pinch_hitter'):
            return None

        qs = User.objects.filter(is_validating=False)

        if self.cleaned_data.get('pinch_hitter').isdigit():
            try:
                return qs.get(pk=self.cleaned_data.get('pinch_hitter'))
            except User.DoesNotExist:
                return None
        else:
            try:
                return qs.get(email=self.cleaned_data.get('pinch_hitter').lower())
            except User.DoesNotExist:
                return None

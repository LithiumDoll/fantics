# Generated by Django 2.1.4 on 2019-01-04 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bid', '0002_auto_20190104_1126'),
    ]

    operations = [
        migrations.AddField(
            model_name='bid',
            name='anonymous',
            field=models.BooleanField(default=False),
        ),
    ]

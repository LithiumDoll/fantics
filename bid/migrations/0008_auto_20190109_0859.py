# Generated by Django 2.1.4 on 2019-01-09 08:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bid', '0007_auto_20190108_1942'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bid',
            old_name='winner_details_email',
            new_name='auctionee_sent_winner_details_email',
        ),
        migrations.RenameField(
            model_name='bid',
            old_name='winner_details_email_sent',
            new_name='auctionee_sent_winner_details_email_date',
        ),
        migrations.RenameField(
            model_name='bid',
            old_name='auctionee_details_email',
            new_name='pinch_hitter_sent_winner_details_email',
        ),
        migrations.RenameField(
            model_name='bid',
            old_name='auctionee_details_email_sent',
            new_name='pinch_hitter_sent_winner_details_email_date',
        ),
        migrations.AddField(
            model_name='bid',
            name='winner_sent_auctionee_details_email',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='bid',
            name='winner_sent_auctionee_details_email_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='bid',
            name='winner_sent_pinch_hitter_details_email',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='bid',
            name='winner_sent_pinch_hitter_details_email_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

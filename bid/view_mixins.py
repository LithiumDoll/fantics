from django.contrib import messages
from django.shortcuts import (redirect, reverse)

from bid.models import Bid


class BidMixin:
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            return redirect(reverse('auction:auction', kwargs={'user': request.auction.user.slug,
                                                               'auction': request.auction.slug}))

        return super().dispatch(request, *args, **kwargs)


class BidRedirectMixin:
    _bid = None

    @property
    def bid(self):
        if self._bid:
            return self._bid

        try:
            self._bid = Bid.objects.get(pk=self.kwargs.get('pk'))
        except Bid.DoesNotExist:
            pass

        return self._bid

    def dispatch(self, request, *args, **kwargs):
        if not self.bid or self.bid.offer.auction_id != request.auction.id:
            messages.error(request, "You can't administrate this bid")
            return reverse('auction:results', kwargs={'user': self.request.auction.user.slug,
                                                      'auction': self.request.auction.slug})

        return super().dispatch(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('auction:results', kwargs={'user': self.request.auction.user.slug,
                                                  'auction': self.request.auction.slug})


class BidAdministrationMixin:
    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)

        if self.object.offer.auction_id != request.auction.id:
            messages.error(request, "You can't administrate this bid")
            return reverse('auction:results', kwargs={'user': request.auction.user.slug,
                                                      'auction': request.auction.slug})

        return response

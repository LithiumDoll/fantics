from django.db import models


class MessageManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('-created').select_related('user').prefetch_related('recipients')


class Message(models.Model):
    user = models.ForeignKey('account.User', related_name='outbox', on_delete=models.SET_NULL, blank=True, null=True)
    recipients = models.ManyToManyField('account.User', through='message.Recipient', related_name='+')
    created = models.DateTimeField(auto_now_add=True)
    subject = models.CharField(max_length=255)
    text = models.TextField()

    objects = MessageManager()

    def __str__(self):
        return self.subject

    def format_recipients(self):
        recipients = [user.display_name for user in self.recipients.all()]

        return ", ".join(recipients)


class RecipientManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()\
            .order_by('-id')\
            .select_related('user')\
            .select_related('message')


class Recipient(models.Model):
    user = models.ForeignKey('account.User', related_name='inbox', on_delete=models.CASCADE)
    message = models.ForeignKey('message.Message', related_name='to', on_delete=models.CASCADE)
    read = models.DateTimeField(null=True, blank=True)
    deleted = models.BooleanField(default=False)

    objects = RecipientManager()

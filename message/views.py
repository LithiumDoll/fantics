from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import (redirect, reverse)
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import (ListView, CreateView, DetailView, DeleteView)

from .forms import MessageForm
from .models import (Message, Recipient)


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageView(DetailView):
    model = Recipient
    template_name = 'message/view.html'

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().user.id != request.user.id or self.get_object().deleted:
            messages.error(request, "The message isn't for you, or has been deleted")
            return redirect(reverse('message:list'))

        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        if not self.object.read:
            self.object.read = timezone.now()
            self.object.save()

        return response


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageCreateView(CreateView):
    model = Message
    template_name = 'message/create.html'
    form_class = MessageForm
    success_url = reverse_lazy('message:list')

    def get_initial(self):
        initial = super().get_initial()

        if self.request.GET.get('to'):
            initial.update({'to': self.request.GET.get('to')})

        elif self.request.GET.get('replyTo') and self.request.GET.get('replyTo').isdigit():
            recipient = Recipient.objects.filter(user=self.request.user, pk=self.request.GET.get('replyTo'))

            if recipient:
                recipient = recipient.get()
                initial.update({
                    'subject': "Re: {}".format(recipient.message.subject),
                    'to': recipient.message.user.display_name,
                    'text': "\n\n\n-------------------\n{}".format(recipient.message.text)
                })

        return initial

    def form_valid(self, form):
        form.instance.user = self.request.user

        response = super().form_valid(form)

        recipients = [Recipient(user=user, message=self.object) for user in form.cleaned_data.get('to')]

        Recipient.objects.bulk_create(recipients)

        messages.success(self.request, "Your message is on its way!")

        return response


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageListView(ListView):
    model = Recipient
    paginate_by = 20
    template_name = 'message/list.html'

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user, deleted=False)


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class MessageDeleteView(DeleteView):
    model = Recipient
    template_name = 'message/delete.html'
    success_url = reverse_lazy('message:list')

    def post(self, request, *args, **kwargs):
        obj = self.get_object()

        obj.deleted = True
        obj.save()

        return redirect(reverse_lazy('message:list'))

from django.conf import settings
from django.contrib.auth.models import (AbstractUser, UserManager as AbstractUserManager)
from django.db import models
from django.db.models.functions import Lower
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from autoslugged import AutoSlugField

from core.helpers import file_name
from message.models import Recipient

from .helpers import ValidatingTokenGenerator


class UserManager(AbstractUserManager):
    def get_queryset(self):
        unread_messages = Recipient.objects\
            .filter(read__isnull=True, user_id=models.OuterRef('pk'))\
            .order_by()\
            .values('user_id')

        return super().get_queryset().annotate(
            count_messages=models.Count('inbox', distinct=True),
            count_unread_messages=models.Subquery(unread_messages.annotate(c=models.Count('*')).values('c'),
                                                  output_field=models.IntegerField())
        ).order_by(Lower('username'))

    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


def user_file_name(instance, filename):
    return '/'.join(['user', 'icons', file_name(filename)])


class User(AbstractUser):
    display_name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='display_name', unique=True, always_update=True)
    is_validating = models.BooleanField(default=False, help_text='E-mail not confirmed')
    opt_in_email = models.BooleanField(default=False)

    website = models.URLField(blank=True, max_length=255)

    tumblr = models.CharField(blank=True, max_length=255)
    twitter = models.CharField(blank=True, max_length=255)
    livejournal = models.CharField(blank=True, max_length=255)
    dreamwidth = models.CharField(blank=True, max_length=255)
    pillowfort = models.CharField(blank=True, max_length=255)
    ao3 = models.CharField(blank=True, max_length=255)
    instagram = models.CharField(blank=True, max_length=255)

    icon = models.ImageField(null=True, blank=True, upload_to=user_file_name)

    objects = UserManager()

    def __str__(self):
        return self.display_name

    def save(self, *args, **kwargs):
        self.email = self.email.lower()

        return super().save(*args, **kwargs)

    def send_email_verification(self, request):
        account_activation_token = ValidatingTokenGenerator()

        message = render_to_string('account/emails/verification.html', {
            'user': self,
            'domain': settings.BASE_URL,
            'uid': urlsafe_base64_encode(force_bytes(self.pk)).decode(),
            'token': account_activation_token.make_token(self),
        })

        self.email_user('Activate Your Account', message)

        return message

import ast


class AccountConsentCookieMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):  # noqa
        cookie_consent = None

        if 'cookie_consent' in request.COOKIES.keys():
            try:
                cookie_consent = ast.literal_eval(request.COOKIES.get('cookie_consent'))
            except ValueError:
                pass

        for key in ['preferences', 'analytics']:
            setattr(request.user, 'allows_{}_cookies'.format(key), True)

            if isinstance(cookie_consent, list) and key not in cookie_consent:
                setattr(request.user, 'allows_{}_cookies'.format(key), False)

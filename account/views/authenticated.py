from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import (PasswordChangeView as _PasswordChangeView, LogoutView as _LogoutView)
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import (reverse, reverse_lazy)
from django.utils.decorators import method_decorator
from django.views.generic import (UpdateView, RedirectView)

from ..forms import (DeleteForm, AccountForm)
from ..models import User


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class AccountDeleteView(UpdateView):
    template_name = 'account/delete.html'
    model = User
    form_class = DeleteForm

    def form_valid(self, form):
        response = super().form_valid(form)

        logout(self.request)

        return response

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('main:home')


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class LogoutView(_LogoutView):
    next_page = reverse_lazy('account:login')


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class PasswordChangeView(SuccessMessageMixin, _PasswordChangeView):
    template_name = 'account/password.html'
    model = User
    success_message = 'Your password has been updated'

    def get_success_url(self):
        return reverse('account:update')


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class AccountUpdateView(SuccessMessageMixin, UpdateView):
    template_name = 'account/account.html'
    model = User
    form_class = AccountForm
    success_message = 'Your account has been updated'

    def get_success_url(self):
        return reverse('account:update')

    def get_object(self, queryset=None):
        return self.request.user


@method_decorator(login_required(login_url=reverse_lazy('account:login')), name='dispatch')
class ResendValidationView(RedirectView):
    def get(self, request, *args, **kwargs):
        if request.user.is_validating:
            request.user.send_email_verification(request)
            messages.success(request, "You'll get a verification e-mail at {} soon".format(request.user.email))
        else:
            messages.warning(request, "Your account is already validated, you're good")

        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('account:login')

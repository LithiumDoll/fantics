from .anonymous import (LoginView, RegisterView, PasswordResetConfirmView, PasswordResetView)
from .authenticated import (PasswordChangeView, AccountUpdateView, AccountDeleteView, LogoutView, ResendValidationView)
from .general import (ValidateView, CookiesView, ProfileView)

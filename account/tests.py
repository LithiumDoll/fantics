from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.shortcuts import reverse
from django.test import (TestCase, RequestFactory, Client, override_settings)
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from .helpers import ValidatingTokenGenerator
from .models import User
from .views import (RegisterView, ValidateView, LoginView, ProfileView)


class AccountTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

        User.objects.create_user("TestUser", "test.user@fantics.me", "password", is_validating=True)


class AccountModelTestCase(AccountTestCase):
    def test_user_str(self):
        user = User.objects.get(pk=1)
        self.assertEqual(user.email, str(user))

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
    def test_send_email_verification_is_validating(self):
        user = User.objects.get(pk=1)
        request = self.factory.get('/')
        request.user = user

        account_activation_token = ValidatingTokenGenerator()
        token = account_activation_token.make_token(user)
        uidb64 = urlsafe_base64_encode(force_bytes(user.pk)).decode()

        message = user.send_email_verification(request)

        url = "{}{}".format(settings.BASE_URL,
                            reverse('account:validate_email', kwargs={'uidb64': uidb64, 'token': token}))

        self.assertIn(url, message)

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
    def test_send_email_verification_is_not_validating(self):
        user = User.objects.get(pk=1)
        user.is_validating = False
        user.save()

        request = self.factory.get('/')
        request.user = user

        self.assertFalse(user.send_email_verification(request))

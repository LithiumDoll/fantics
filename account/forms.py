from django import forms
from django.contrib.auth.forms import (AuthenticationForm, UserCreationForm, UserChangeForm)
from django.shortcuts import reverse
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _

from .models import User


class LoginForm(AuthenticationForm):
    remember = forms.BooleanField(widget=forms.CheckboxInput(), initial=False,
                                  label=_('Remember me'), required=False)


class RegisterForm(UserCreationForm):
    email = forms.EmailField(widget=forms.EmailInput())
    age = forms.BooleanField(widget=forms.CheckboxInput(), label="I'm 13 or older")
    terms_of_use = forms.BooleanField(widget=forms.CheckboxInput(), label="")

    def clean_display_name(self):
        user = User.objects.filter(display_name__iexact=self.cleaned_data.get('display_name'))

        if user.count():
            raise forms.ValidationError('A user with this display name already exists',
                                        'not_unique',
                                        params=['display_name', ])

        return self.cleaned_data.get('display_name')

    class Meta:
        model = User
        fields = ('email', 'username', 'display_name', 'password1', 'password2', 'age', 'terms_of_use')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['terms_of_use'].label = mark_safe("I agree with the <a href='{}' target='_blank'>Terms of Use</a>".format(
            reverse('main:terms_of_use'))
        )


class AccountForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput)
    website = forms.URLField(widget=forms.URLInput(attrs={'placeholder': 'http://www.yourwebsite.com'}), required=False)

    class Meta:
        model = User
        fields = ['email', 'username', 'display_name', 'tumblr', 'website', 'twitter', 'pillowfort',
                  'dreamwidth', 'ao3', 'livejournal', 'icon']

    def clean_display_name(self):
        user = User.objects.\
            filter(display_name__iexact=self.cleaned_data.get('display_name'))\
            .exclude(pk=self.instance.pk)

        if user.count():
            raise forms.ValidationError('A user with this display name already exists',
                                        'not_unique',
                                        params=['display_name', ])

        return self.cleaned_data.get('display_name')


class DeleteForm(forms.ModelForm):
    is_active = forms.BooleanField(widget=forms.HiddenInput)

    class Meta:
        model = User
        fields = ['is_active', ]

    def clean_is_active(self):
        return False


class CookieForm(forms.Form):
    necessary = forms.BooleanField(widget=forms.CheckboxInput(attrs={'checked': True, 'disabled': True}),
                                   required=False)
    preferences = forms.BooleanField(widget=forms.CheckboxInput, required=False)
    analytics = forms.BooleanField(widget=forms.CheckboxInput, required=False)

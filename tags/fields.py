from django import forms

from taggit.utils import parse_tags

from .widgets import FanticsTagWidget


class FanticsTagField(forms.CharField):
    widget = FanticsTagWidget

    def clean(self, value):
        value = super().clean(value)
        try:
            return parse_tags(value)
        except ValueError:
            raise forms.ValidationError("Please provide a comma-separated list of tags.")

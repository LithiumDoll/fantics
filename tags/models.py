from django.db import models
from django.utils.translation import ugettext_lazy as _

from taggit.models import (TagBase, GenericTaggedItemBase)

from core.helpers import EnumChoices


class FanticsTag(TagBase):
    class MODE(EnumChoices):
        general = (0, 'General')
        character = (1, 'Character')
        relationship = (2, 'Relationship')
        category = (3, 'Category')
        fandom = (4, 'Fandom')

    mode = models.IntegerField(default=0, db_index=True)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class FanticsTaggedItem(GenericTaggedItemBase):
    tag = models.ForeignKey(FanticsTag, related_name="%(app_label)s_%(class)s_items", on_delete=models.CASCADE)

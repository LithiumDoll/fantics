from taggit.utils import _parse_tags


"""
Removes the split-on-space behaviour for single tags. See taggit.utils._parse_tags for comments
"""


def parse_tags(tagstring):
    if not tagstring:
        return []

    if ',' not in tagstring and '"' not in tagstring:
        words = [tagstring, ]
        return words

    return _parse_tags(tagstring)

import sys
import os
import django

from channels.routing import get_default_application


INTERP = "/home/fantics/venv/uat/bin/python3.6"

if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)


cwd = os.getcwd()
sys.path.append(cwd)
sys.path.append('{}/fantics'.format(cwd))


sys.path.insert(0, "/home/fantics/venv/uat/bin")
sys.path.insert(0, "/home/fantics/venv/uat/bin/python3.6/site-packages")


os.environ['DJANGO_SETTINGS_MODULE'] = "fantics.settings"

django.setup()

application = get_default_application()

import os

from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured


if os.path.isfile(os.path.join('fantics', '.env')):
    try:
        with open(os.path.join('fantics', '.env'), 'r') as env_file:
            for var in env_file.read().split("\n"):
                os.environ.setdefault(var.split('=')[0], var.split('=')[1])

    except FileNotFoundError:
        from django.core.exceptions import ImproperlyConfigured
        raise ImproperlyConfigured("Couldn't find an .env file")


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_URL = os.environ.get('BASE_URL')


try:
    SECRET_KEY = os.environ['SECRET_KEY']
except KeyError:
    raise ImproperlyConfigured("No secret key")


DEBUG = bool(int(os.environ.get('DEBUG', 0)))

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS').split(',')

APPEND_SLASH = True

LOGIN_URL = "/login/"

ENVIRONMENT = os.environ.get('ENVIRONMENT')

# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap4',
    'channels',
    'ckeditor',
    'django_cleanup',
    'encrypted_model_fields',
    'hijack',
    'rest_framework',
    'taggit',
    'account',
    'api',
    'auction',
    'bid',
    'category',
    'core',
    'main',
    'message',
    'offer',
    'tags'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.CurrencyMiddleware',
    'account.middleware.AccountConsentCookieMiddleware',
    'auction.middleware.AuctionMiddleware',
]

if DEBUG:
    INSTALLED_APPS.append('debug_toolbar')
    INSTALLED_APPS.append('django_jenkins')
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
    INTERNAL_IPS = [os.environ.get('ALLOWED_HOSTS')]

ROOT_URLCONF = 'fantics.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'fantics', 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.environment_settings',
            ],
        },
    },
]

WSGI_APPLICATION = 'fantics.wsgi.application'


# Logging
LOGGING = {}


# Cache
CACHES = {
    'default': {
        'BACKEND': os.environ.get('CACHE_BACKEND'),
        'LOCATION': os.environ.get('CACHE_LOCATION'),
    },
}


# Cookies
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_PATH = "/"
SESSION_COOKIE_DOMAIN = os.environ.get('SESSION_COOKIE_DOMAIN', None)


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

if DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DATABASE_ENGINE'),
            'NAME': os.path.join(BASE_DIR, 'fantics', os.environ.get('DATABASE_NAME')),
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DATABASE_ENGINE'),
            'NAME': os.environ.get('DATABASE_NAME'),
            'USER': os.environ.get('DATABASE_USER'),
            'PASSWORD': os.environ.get('DATABASE_PASSWORD'),
            'HOST': os.environ.get('DATABASE_HOST'),
            'PORT': int(os.environ.get('DATABASE_PORT', 3306)),
            'CONN_MAX_AGE': int(os.environ.get('CONN_MAX_AGE', 60)),
            'TEST': {
                'CHARSET': 'utf8',
                'COLLATION': 'utf8_general_ci',
            }
        },
    }

if os.environ.get('DATABASE_ENGINE') == 'django.db.backends.mysql':
    DATABASES['default']['OPTIONS'] = {
        'init_command': "SET "
                        "sql_mode='STRICT_TRANS_TABLES',"
                        "storage_engine=INNODB,"
                        "character_set_connection=utf8,"
                        "collation_connection=utf8_unicode_ci",
    }


# Account
AUTH_USER_MODEL = 'account.User'

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

if DEBUG:
    STATIC_ROOT = os.path.join(BASE_DIR, 'fantics', 'static')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'fantics', 'media')
else:
    STATIC_ROOT = os.path.join(BASE_DIR, 'public', 'static')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'public', 'media')


# Message settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}


# Email
EMAIL_BACKEND = os.environ.get('EMAIL_BACKEND')
DEFAULT_FROM_EMAIL = "botling@fantics.me"

if os.environ.get('EMAIL_HOST'):
    EMAIL_HOST = os.environ.get('EMAIL_HOST')
    EMAIL_PORT = int(os.environ.get('EMAIL_PORT', 587))
    EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
    EMAIL_USE_TLS = bool(int(os.environ.get('EMAIL_USE_TLS', 0)))
    EMAIL_USE_SSL = bool(int(os.environ.get('EMAIL_USE_SSL', 0)))


# Lib settings
ASGI_APPLICATION = "fantics.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}

BOOTSTRAP4 = {
    'include_jquery': True,
    'jquery_url': '//code.jquery.com/jquery-3.2.1.min.js',
    'required_css_class': 'required'
}

CKEDITOR_CONFIGS = {
    'default': {
        'language': 'en',
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft',
             'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ]
    }
}

FIELD_ENCRYPTION_KEY = "ct-DLSBBEgPSYH1Turk_4EMr2v1MLxjZfIRgaMWae_4="

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

TAGGIT_CASE_INSENSITIVE = True
TAGGIT_TAGS_FROM_STRING = 'tags.helpers.parse_tags'

OPEN_EXCHANGE_APP_ID = "18e914d5dac74e5a930cc51adc89aa8e"

from channels.auth import AuthMiddlewareStack
from channels.routing import (ProtocolTypeRouter, URLRouter)

import offer.routing


application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter(
            offer.routing.websocket_urlpatterns
        )
    ),
})

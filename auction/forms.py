from django import forms

from ckeditor.widgets import CKEditorWidget

from .models import Auction


class AuctionForm(forms.ModelForm):
    signup_starts = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)
    signup_ends = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)
    bidding_starts = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)
    bidding_ends = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)
    offer_deadline = forms.DateField(input_formats=["%m/%d/%Y", ], required=False)
    donation_deadline = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)

    currency = forms.ChoiceField(widget=forms.Select, choices=[])

    beneficiary_name = forms.CharField(widget=forms.TextInput, label='Name')
    beneficiary_description = forms.CharField(widget=forms.Textarea, required=False, label="Description")
    beneficiary_website = forms.URLField(widget=forms.URLInput(attrs={'placeholder': 'http://'}), label='Website',
                                         required=False)
    beneficiary_image = forms.ImageField(label='Image', required=False)
    beneficiary_donation_website = forms.URLField(widget=forms.URLInput(attrs={'placeholder': 'http://'}),
                                                  label='Donation website', required=False)
    beneficiary_donation_information = forms.CharField(widget=forms.Textarea, label='Donation instructions',
                                                       required=False)
    max_offers_per_user = forms.IntegerField(widget=forms.NumberInput, label='Max. offers per user', required=False,
                                             help_text='Leave blank for no maximum')
    max_quantity_per_offer = forms.IntegerField(widget=forms.NumberInput, label='Max. quantity per offer',
                                                required=False, help_text='Leave blank for no maximum')

    about = forms.CharField(widget=CKEditorWidget(), required=False)
    target = forms.IntegerField(widget=forms.NumberInput, required=True, help_text='Leave at 0 for no set target')

    class Meta:
        model = Auction
        fields = ['title', 'contact_name', 'contact_email', 'is_listed', 'target',
                  'beneficiary_name', 'beneficiary_website', 'beneficiary_image', 'beneficiary_donation_website',
                  'beneficiary_donation_information', 'beneficiary_description',
                  'signup_starts', 'signup_ends', 'bidding_starts', 'bidding_ends', 'offer_deadline',
                  'allow_custom_offer_deadline', 'about', 'currency', 'timezone', 'donation_deadline',
                  'max_offers_per_user', 'max_quantity_per_offer']

    def clean(self):
        data = super().clean()

        if data.get('signup_starts') and data.get('signup_ends') \
                and data.get('signup_ends') < data.get('signup_starts'):
            raise forms.ValidationError('Sign ups must end after they start', 'invalid',
                                        params=['signup_starts', 'signup_ends'])

        if data.get('bidding_starts') and data.get('bidding_ends') \
                and data.get('bidding_ends') < data.get('bidding_starts'):
            raise forms.ValidationError('Bidding must end after it starts', 'invalid',
                                        params=['bidding_starts', 'bidding_ends'])

        if data.get('signup_ends') and data.get('bidding_starts') \
                and data.get('bidding_starts') < data.get('signup_ends'):
            raise forms.ValidationError('Bidding must begin after sign ups end', 'invalid',
                                        params=['bidding_starts', 'signup_ends'])

        if data.get('offer_deadline') and data.get('bidding_ends') \
                and data.get('offer_deadline') < data.get('bidding_ends').date():
            raise forms.ValidationError('The offer deadline must be after bidding ends', 'invalid',
                                        params=['offer_deadline', 'bidding_ends'])

        if not data.get('beneficiary_donation_website') and not data.get('beneficiary_donation_information'):
            raise forms.ValidationError('Please give at least one of donation website or donation instructions',
                                        'invalid',
                                        params=['beneficiary_donation_website', 'beneficiary_donation_information'])

        return data


class AuctionEmailForm(forms.ModelForm):
    signup_email_body = forms.CharField(widget=CKEditorWidget())
    bid_email_body = forms.CharField(widget=CKEditorWidget())
    overbid_email_body = forms.CharField(widget=CKEditorWidget())
    winner_alert_email_body = forms.CharField(widget=CKEditorWidget())
    winner_details_to_auctionee_email_body = forms.CharField(widget=CKEditorWidget())
    auctionee_details_to_winner_email_body = forms.CharField(widget=CKEditorWidget())
    pinch_hitter_details_to_winner_email_body = forms.CharField(widget=CKEditorWidget())
    winner_details_to_pinch_hitter_email_body = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Auction
        fields = ['signup_email_body', 'signup_email_subject', 'bid_email_body', 'bid_email_subject',
                  'overbid_email_body', 'overbid_email_subject', 'winner_alert_email_body',
                  'winner_alert_email_subject',
                  'winner_details_to_auctionee_email_body', 'winner_details_to_auctionee_email_subject',
                  'auctionee_details_to_winner_email_body', 'auctionee_details_to_winner_email_subject',
                  'pinch_hitter_details_to_winner_email_body', 'pinch_hitter_details_to_winner_email_subject',
                  'winner_details_to_pinch_hitter_email_body', 'winner_details_to_pinch_hitter_email_subject']

    def _email_validator(self, field, label, kwargs):
        tags = ["{"+key+"}" for key in kwargs.keys()]

        try:
            self.cleaned_data.get(field).format(**kwargs)

        except (ValueError, KeyError, AttributeError):
            raise forms.ValidationError(
                "'{}' isn't parsing correctly, does it contain {}?".format(label, ", ".join(tags)),
                'invalid',
                params=[field]
            )

        return self.cleaned_data.get(field)

    def clean_signup_email_body(self):
        return self._email_validator('signup_email_body', "After adding an offer to the auction",
                                     {'auctionee': "", 'category': "", 'link': "", 'quantity': ""})

    def clean_bid_email_body(self):
        return self._email_validator('bid_email_body', "When offer is bid on",
                                     {'auctionee': "", 'category': "", 'bidder': "", 'amount': ""})

    def clean_overbid_email_body(self):
        return self._email_validator('overbid_email_body', "When over-bid", {'auctionee': "", 'bidder': "", 'link': ""})

    def clean_winner_alert_email_body(self):
        return self._email_validator('winner_alert_email_body', "On win, with donation instructions",
                                     {'auctionee': "", 'category': "", 'bidder': "", 'amount': "",
                                      'donation_instructions': "", 'donation_website': "", 'deadline': "", 'link': ""})

    def clean_winner_details_to_auctionee_email_body(self):
        return self._email_validator('winner_details_to_auctionee_email_body',
                                     "After offer has been verified as donated for",
                                     {'auctionee': "", 'category': "", 'bidder': "", 'amount': "", 'email': ""})

    def clean_auctionee_details_to_winner_email_body(self):
        return self._email_validator('auctionee_details_to_winner_email_body',
                                     "After donation has been verified",
                                     {'auctionee': "", 'category': "", 'bidder': "", 'amount': "", 'email': ""})

    def clean_winner_details_to_pinch_hitter_email_body(self):
        return self._email_validator('winner_details_to_pinch_hitter_email_body',
                                     "Sent to pinch-hitter after pinch-hitter assigned",
                                     {'pinch_hitter': "", 'category': "", 'bidder': "", 'email': ""})

    def clean_pinch_hitter_details_to_winner_email_body(self):
        return self._email_validator('pinch_hitter_details_to_winner_email_body',
                                     "Sent to winner after pinch-hitter assigned",
                                     {'pinch_hitter': "", 'category': "", 'bidder': "", 'email': "", 'auctionee': ""})


class AuctionLayoutForm(forms.ModelForm):
    class Meta:
        model = Auction
        fields = ['icon', 'logo', 'banner', 'css']

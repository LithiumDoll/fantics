from django import forms
from django.contrib import messages
from django.shortcuts import (reverse, redirect)
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, UpdateView, DeleteView, FormView, DetailView)

from bid.models import Bid
from offer.forms import SearchForm

from .decorators import auction_owner_required
from .forms import (AuctionForm, AuctionEmailForm, AuctionLayoutForm)
from .models import Auction
from .view_mixins import (AuctionMixin, AuctionSingleObjectMixin)


class AuctionView(FormView, DetailView):
    model = Auction
    form_class = SearchForm
    template_name = 'auction/auction.html'

    def _offers(self):
        if not self.request.auction.count_offers:
            return []

        form = SearchForm({'search': self.request.GET.get('search')})

        if form.is_valid():
            return self.request.auction.offers.filter(notes__contains=form.cleaned_data.get('search'))
        else:
            return self.request.auction.offers.all()

    def form_valid(self, form):
        path = self.request.path

        if '?search' in path:
            path = path.split('?')[0]

        return redirect("{}?search={}".format(path, form.cleaned_data.get('search')))

    def get_object(self, queryset=None):
        return self.request.auction

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.object.complete:
            categories_data = {}
            for bid in self.object.top_bids:
                if bid.offer.category.slug not in categories_data.keys():
                    categories_data[bid.offer.category.slug] = {'total': 0, 'donated': 0}

                categories_data[bid.offer.category.slug]['total'] += bid.amount

                if bid.donation_complete:
                    categories_data[bid.offer.category.slug]['donated'] += bid.amount

            for category in data['auction'].categories.all():
                if category.slug in categories_data:
                    category.total = categories_data[category.slug]['total']
                    category.donated = categories_data[category.slug]['donated']
                    category.percentage = (100 / categories_data[category.slug]['total']) * categories_data[category.slug]['donated']

        else:
            data.update({
                'offer_list': self._offers(),
                'keywords': self.request.GET.get('search'),
                'form_action': reverse('auction:auction', kwargs={
                    'user': self.request.auction.user.slug,
                    'auction': self.request.auction.slug
                })
            })

        return data


class AuctionAboutView(AuctionMixin, AuctionSingleObjectMixin, DetailView):
    model = Auction
    template_name = 'auction/about.html'


class AuctionCreateView(CreateView):
    model = Auction
    template_name = 'auction/administration/create.html'
    fields = ['title', 'beneficiary_name', 'contact_name', 'contact_email', 'is_listed']

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            messages.info(request, 'Sorry, you need to be logged in to create an auction')
            return redirect('{}?next={}'.format(reverse('account:login'), reverse('auction:create', kwargs={
                'user': request.user.slug
            })))

        elif request.user.slug != self.kwargs.get('user'):
            return redirect(reverse('auction:create', kwargs={'user': request.user.slug}))

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('auction:update', kwargs={'user': self.request.user.slug, 'auction': self.object.slug})

    def form_valid(self, form):
        form.instance.user = self.request.user

        return super().form_valid(form)


@method_decorator(auction_owner_required, name='dispatch')
class AuctionUpdateView(AuctionMixin, AuctionSingleObjectMixin, UpdateView):
    model = Auction
    template_name = 'auction/administration/update.html'
    form_class = AuctionForm

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)

        form.fields['currency'].choices = [(c.code, c.code) for c in self.request.currency]

        if self.object.count_offers:
            form.fields['currency'].widget = forms.HiddenInput()
            form.fields['timezone'].widget = forms.HiddenInput()
            form.fields['beneficiary_name'].widget = forms.HiddenInput()

        return form

    def form_valid(self, form):
        messages.success(self.request, 'Your auction settings have been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('auction:update', kwargs={'user': self.object.user.slug, 'auction': self.object.slug})


@method_decorator(auction_owner_required, name='dispatch')
class AuctionUpdateEmailView(AuctionMixin, AuctionSingleObjectMixin, UpdateView):
    model = Auction
    template_name = 'auction/administration/update_email.html'
    form_class = AuctionEmailForm

    def get_initial(self):
        initial = super().get_initial()

        if not self.object.signup_email_subject:
            initial.update({
                'signup_email_subject': 'Thank you for signing up to {{auction}}!',
                'signup_email_body': '<p>Hi, {{auctionee}}!</p>'
            })

        return initial

    def form_valid(self, form):
        messages.success(self.request, 'Your auction e-mails have been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('auction:update_email', kwargs={'user': self.object.user.slug, 'auction': self.object.slug})


@method_decorator(auction_owner_required, name='dispatch')
class AuctionUpdateLayoutView(AuctionMixin, AuctionSingleObjectMixin, UpdateView):
    model = Auction
    template_name = 'auction/administration/update_layout.html'
    form_class = AuctionLayoutForm

    def form_valid(self, form):
        messages.success(self.request, 'Your auction layout has been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('auction:update_layout', kwargs={'user': self.object.user.slug, 'auction': self.object.slug})


@method_decorator(auction_owner_required, name='dispatch')
class AuctionDeleteView(AuctionMixin, AuctionSingleObjectMixin, DeleteView):
    model = Auction
    template_name = 'auction/administration/delete.html'

    def get_success_url(self):
        return reverse('account:profile', kwargs={'slug': self.request.user.slug})

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()

        if obj.donated:
            messages.error(self.request, "Your auction has donations made against it - it can't be deleted")
            return redirect(reverse('auction:update', kwargs={'user': obj.user.slug, 'auction': obj.slug}))

        return super().dispatch(request, *args, **kwargs)


@method_decorator(auction_owner_required, name='dispatch')
class AuctionResultsView(AuctionMixin, AuctionSingleObjectMixin, DetailView):
    model = Auction
    template_name = 'auction/administration/results.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.auction.show_nav_wins:
            return redirect(reverse('auction:update', kwargs={'user': self.request.auction.user.slug,
                                                              'auction': self.request.auction.slug}))

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        filters = dict.fromkeys(Bid.MODES, 0)

        top_bids = self.request.auction.top_bids

        for bid in self.request.auction.top_bids:
            filters[bid.status] += 1

        if self.request.GET.get('filter') in Bid.MODES:
            top_bids = [bid for bid in self.request.auction.top_bids if bid.status == self.request.GET.get('filter')]

        data.update({'top_bids': top_bids, 'filters': filters})

        return data

from django.http import Http404
from django.utils.functional import cached_property

from .models import Auction


class AuctionMixin:
    @cached_property
    def auction(self):
        try:
            if self.request.auction:
                return self.request.auction

        except AttributeError:
            pass

        _auction = None

        if self.kwargs.get('auction') and self.kwargs.get('user'):
            try:
                _auction = Auction.objects.filter(slug=self.kwargs.get('auction'),
                                                  user__slug=self.kwargs.get('user')).get()
            except Auction.DoesNotExist:
                pass

        return _auction

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.auction:
            data.update({'auction': self.auction})

        return data


class AuctionSingleObjectMixin:
    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()

        if not self.auction:
            raise Http404("No {} found matching the query".format(queryset.model._meta.verbose_name))

        return self.auction

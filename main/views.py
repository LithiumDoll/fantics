from django.views.generic import ListView
from django.utils import timezone

from auction.models import Auction


class HomeView(ListView):
    template_name = 'main/brochure/home.html'
    model = Auction

    def get_queryset(self):
        return super().get_queryset().filter(bidding_ends__gte=timezone.now(), is_listed=True)

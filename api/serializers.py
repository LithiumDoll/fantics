from rest_framework import serializers

from account.models import User
from auction.models import Auction
from bid.models import Bid
from category.models import Category
from offer.models import Offer


class AuctionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Auction
        fields = ['pk', 'user', 'title', 'offers']


class BidSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bid
        fields = ['pk', 'user', 'pinch_hitter', 'anonymous', 'offer', 'amount', 'created', 'emailed', 'donated', 'receipt', 'has_donated']


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ['pk', 'name', 'description', 'icon', 'offers']


class OfferSerializer(serializers.HyperlinkedModelSerializer):
    bids = BidSerializer(many=True, read_only=True)

    class Meta:
        model = Offer
        fields = ['pk', 'user', 'auction', 'category', 'notes', 'deadline', 'minimum_bid', 'quantity', 'bids']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'display_name']

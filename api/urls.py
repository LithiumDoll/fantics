from django.urls import (path, include)
from rest_framework import routers

from .views import (OfferViewSet, UserViewSet, AuctionViewSet, BidViewSet, CategoryViewSet)


router = routers.DefaultRouter()
router.register(r'auctions', AuctionViewSet)
router.register(r'bids', BidViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_framework.urls', namespace='rest_framework'))
]

from django.urls import path

from .views import (OfferView, OfferCreateView, OfferListView, OfferUpdateView, OfferDeleteView,
                    OfferListAdministrationView, OfferUpdateAdministrationView, OfferDeleteAdministrationView,
                    OfferSearchView)


app_name = 'offer'

urlpatterns = [
    path('<slug:user>/<slug:auction>/create-offer/', OfferCreateView.as_view(), name='create'),
    path('<slug:user>/<slug:auction>/offers/', OfferListAdministrationView.as_view(), name='list_administration'),
    path('<slug:user>/<slug:auction>/offers/<slug:category>/', OfferListAdministrationView.as_view(),
         name='list_administration_category'),
    path('<slug:user>/<slug:auction>/offer/<int:pk>/edit/', OfferUpdateAdministrationView.as_view(),
         name='update_administration'),
    path('<slug:user>/<slug:auction>/offer/<int:pk>/delete/', OfferDeleteAdministrationView.as_view(),
         name='delete_administration'),
    path('<slug:user>/<slug:auction>/tag:<slug:tag>/', OfferSearchView.as_view(), name='search_tag'),
    path('<slug:user>/<slug:auction>/category:<slug:category>/', OfferSearchView.as_view(), name='search_category'),
    path('<slug:user>/<slug:auction>/category:<slug:category>/tag:<slug:tag>/', OfferSearchView.as_view(),
         name='search_items'),
    path('<slug:user>/<slug:auction>/<slug:auctionee>/', OfferListView.as_view(), name='list'),
    path('<slug:user>/<slug:auction>/<slug:auctionee>/<int:pk>/', OfferView.as_view(), name='view'),
    path('<slug:user>/<slug:auction>/<slug:auctionee>/<int:pk>/edit/', OfferUpdateView.as_view(), name='update'),
    path('<slug:user>/<slug:auction>/<slug:auctionee>/<int:pk>/delete/', OfferDeleteView.as_view(), name='delete'),
]

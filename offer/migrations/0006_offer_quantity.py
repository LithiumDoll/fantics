# Generated by Django 2.1.4 on 2019-01-04 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offer', '0005_auto_20190104_1506'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='quantity',
            field=models.IntegerField(default=1),
        ),
    ]

# Generated by Django 2.1.4 on 2019-01-04 13:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0002_auto_20190104_1345'),
        ('offer', '0002_auto_20190104_1126'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='offer',
            name='auction',
        ),
        migrations.AddField(
            model_name='offer',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='offers', to='category.Category'),
            preserve_default=False,
        ),
    ]

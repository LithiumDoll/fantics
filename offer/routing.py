from django.urls import path

from .consumers import OfferConsumer


websocket_urlpatterns = [
    path('offer/<int:offer_id>/', OfferConsumer),
]

from django.urls import path

from .views import (CategoryCreateView, CategoryListView, CategoryUpdateView, CategoryDeleteView)


app_name = 'category'

urlpatterns = [
    path('<slug:user>/<slug:auction>/create-category/', CategoryCreateView.as_view(), name='create'),
    path('<slug:user>/<slug:auction>/categories/', CategoryListView.as_view(), name='list'),
    path('<slug:user>/<slug:auction>/categories/<slug:category>/edit/', CategoryUpdateView.as_view(), name='update'),
    path('<slug:user>/<slug:auction>/categories/<slug:category>/delete/', CategoryDeleteView.as_view(), name='delete')
]

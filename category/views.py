from django.contrib import messages
from django.shortcuts import (reverse, redirect)
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, UpdateView, DeleteView, ListView)

from auction.decorators import auction_owner_required
from auction.view_mixins import AuctionMixin

from .models import Category
from .view_mixins import CategoryMixin


@method_decorator(auction_owner_required, name='dispatch')
class CategoryListView(AuctionMixin, ListView):
    model = Category
    template_name = 'category/list.html'


@method_decorator(auction_owner_required, name='dispatch')
class CategoryCreateView(AuctionMixin, CreateView):
    model = Category
    template_name = 'category/create.html'
    fields = ['name', 'description', 'icon']

    def get_success_url(self):
        return reverse('category:list', kwargs={'user': self.auction.user.slug, 'auction': self.auction.slug})

    def form_valid(self, form):
        form.instance.auction = self.auction

        return super().form_valid(form)


@method_decorator(auction_owner_required, name='dispatch')
class CategoryUpdateView(CategoryMixin, UpdateView):
    model = Category
    template_name = 'category/update.html'
    fields = ['name', 'description', 'icon']

    def form_valid(self, form):
        messages.success(self.request, 'Your category settings have been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('category:list', kwargs={'user': self.auction.user.slug, 'auction': self.auction.slug})


@method_decorator(auction_owner_required, name='dispatch')
class CategoryDeleteView(CategoryMixin, DeleteView):
    model = Category
    template_name = 'category/delete.html'

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().count_offers:
            messages.error(self.request, "Sorry, you can't delete a category that has offers associated with it")
            return redirect(reverse('category:list', kwargs={'user': self.auction.user.slug,
                                                             'auction': self.auction.slug}))

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('category:list', kwargs={
            'user': self.auction.user.slug,
            'auction': self.auction.slug,
        })

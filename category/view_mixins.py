from django.http import Http404
from django.utils.functional import cached_property

from auction.view_mixins import AuctionMixin


class CategoryMixin(AuctionMixin):
    @cached_property
    def category(self):
        _category = None

        if self.auction.count_categories:
            for category in self.auction.categories.all():
                if category.slug == self.kwargs.get('category'):
                    _category = category
                    break

        return _category

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()

        if not self.category:
            raise Http404("No {} found matching the query".format(queryset.model._meta.verbose_name))

        return self.category

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.category:
            data.update({'category': self.category})

        return data
